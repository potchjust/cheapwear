.PHONY: up-all up-api up-front up-admin deploy

up-all: up-api up-front

up-api:
	cd src/api &&  echo "Starting api"; yarn start

up-front:
	 cd src/webapp && echo "starting frontend stuff"; yarn dev

up-admin:
	cd src/api && echo  "starting the api"

shutdown-all:
	echo "Find a way to quit from the command line"

deploy:
	 echo  "for deployment" && cd deploy/ && ansible-playbook -i hosts install.yml --extra-vars "ansible_ssh_user=root ansible_ssh_pass=+WN&qb4m62kdRBi" --ask-vault-pass
